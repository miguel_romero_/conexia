-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 14-04-2019 a las 11:37:21
-- Versión del servidor: 5.5.54-0+deb8u1
-- Versión de PHP: 5.6.38-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `conexia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Camarero`
--

CREATE TABLE IF NOT EXISTS `Camarero` (
`idCamarero` int(11) NOT NULL,
  `Nombre` varchar(40) NOT NULL,
  `Apellido1` varchar(40) DEFAULT NULL,
  `Apellido2` varchar(40) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cliente`
--

CREATE TABLE IF NOT EXISTS `Cliente` (
`idCliente` int(11) NOT NULL,
  `Nombre` varchar(40) NOT NULL,
  `Apellido1` varchar(40) DEFAULT NULL,
  `Apellido2` varchar(40) DEFAULT NULL,
  `Observaciones` varchar(255) NOT NULL DEFAULT ' '
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cocinero`
--

CREATE TABLE IF NOT EXISTS `Cocinero` (
`idCocinero` int(11) NOT NULL,
  `Nombre` varchar(40) NOT NULL,
  `Apellido1` varchar(40) DEFAULT NULL,
  `Apellido2` varchar(40) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DetalleFactura`
--

CREATE TABLE IF NOT EXISTS `DetalleFactura` (
`idDetalleFactura` int(11) NOT NULL,
  `idFactura` int(11) NOT NULL,
  `idCocinero` int(11) NOT NULL,
  `Plato` varchar(255) NOT NULL,
  `Importe` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Factura`
--

CREATE TABLE IF NOT EXISTS `Factura` (
`idFactura` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idCamarero` int(11) NOT NULL,
  `idMesa` int(11) NOT NULL,
  `fechaFactura` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Mesa`
--

CREATE TABLE IF NOT EXISTS `Mesa` (
`idMesa` int(11) NOT NULL,
  `numMaxComensales` int(2) NOT NULL,
  `Ubicacion` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Camarero`
--
ALTER TABLE `Camarero`
 ADD PRIMARY KEY (`idCamarero`);

--
-- Indices de la tabla `Cliente`
--
ALTER TABLE `Cliente`
 ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `Cocinero`
--
ALTER TABLE `Cocinero`
 ADD PRIMARY KEY (`idCocinero`);

--
-- Indices de la tabla `DetalleFactura`
--
ALTER TABLE `DetalleFactura`
 ADD PRIMARY KEY (`idDetalleFactura`), ADD KEY `detalle_factura_factura` (`idFactura`), ADD KEY `detalle_factura_cocinero` (`idCocinero`);

--
-- Indices de la tabla `Factura`
--
ALTER TABLE `Factura`
 ADD PRIMARY KEY (`idFactura`), ADD KEY `factura_camarero` (`idCamarero`), ADD KEY `factura_mesa` (`idMesa`), ADD KEY `factura_cliente` (`idCliente`);

--
-- Indices de la tabla `Mesa`
--
ALTER TABLE `Mesa`
 ADD PRIMARY KEY (`idMesa`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Camarero`
--
ALTER TABLE `Camarero`
MODIFY `idCamarero` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `Cliente`
--
ALTER TABLE `Cliente`
MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT de la tabla `Cocinero`
--
ALTER TABLE `Cocinero`
MODIFY `idCocinero` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `DetalleFactura`
--
ALTER TABLE `DetalleFactura`
MODIFY `idDetalleFactura` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `Factura`
--
ALTER TABLE `Factura`
MODIFY `idFactura` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `Mesa`
--
ALTER TABLE `Mesa`
MODIFY `idMesa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `DetalleFactura`
--
ALTER TABLE `DetalleFactura`
ADD CONSTRAINT `DetalleFactura_ibfk_1` FOREIGN KEY (`idFactura`) REFERENCES `Factura` (`idFactura`),
ADD CONSTRAINT `DetalleFactura_ibfk_2` FOREIGN KEY (`idCocinero`) REFERENCES `Cocinero` (`idCocinero`);

--
-- Filtros para la tabla `Factura`
--
ALTER TABLE `Factura`
ADD CONSTRAINT `Factura_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `Cliente` (`idCliente`),
ADD CONSTRAINT `Factura_ibfk_2` FOREIGN KEY (`idCamarero`) REFERENCES `Camarero` (`idCamarero`),
ADD CONSTRAINT `Factura_ibfk_3` FOREIGN KEY (`idMesa`) REFERENCES `Mesa` (`idMesa`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
