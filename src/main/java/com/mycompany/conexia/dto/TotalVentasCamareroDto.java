/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.dto;

import com.mycompany.conexia.entity.Camarero;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Getter
@Setter
@AllArgsConstructor
public class TotalVentasCamareroDto implements Serializable{

    private String mes;
    private Camarero camarero;
    private Long cantidad;
}
