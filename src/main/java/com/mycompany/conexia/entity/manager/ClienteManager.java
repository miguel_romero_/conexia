/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.entity.manager;

import com.mycompany.conexia.entity.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class ClienteManager {

    @PersistenceContext
    private EntityManager entityManager;
    
    public Cliente createCliente(Cliente cliente){        
        this.entityManager.persist(cliente);
        this.entityManager.flush(); 
        return cliente;
    }
    
    public List<Cliente> getGoodClients(){
        TypedQuery<Cliente> query = this.entityManager.createNamedQuery("Cliente.getGoodClients", Cliente.class);
        return query.getResultList();
    }
    
    
}
