/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.entity.manager;

import com.mycompany.conexia.entity.Mesa;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Stateless
public class MesaManager {
    
    @PersistenceContext
    private EntityManager entityManager;
    
     public Mesa createMesa(Mesa mesa){
        this.entityManager.persist(mesa);
        this.entityManager.flush();
        return mesa;        
    }
    
    public List<Mesa> findAllMesa(){
        TypedQuery<Mesa> query = this.entityManager.createNamedQuery("Mesa.findAll", Mesa.class);
        return query.getResultList();
    }

}
