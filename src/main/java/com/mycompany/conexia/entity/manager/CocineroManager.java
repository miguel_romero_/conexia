/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.conexia.entity.manager;

import com.mycompany.conexia.entity.Cocinero;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Stateless
public class CocineroManager {

    @PersistenceContext
    private EntityManager entityManager;
    
    public Cocinero createCocinero(Cocinero cocinero){
        this.entityManager.persist(cocinero);
        this.entityManager.flush();
        return cocinero;        
    }
    
    public List<Cocinero> findAllCocinero(){
        TypedQuery<Cocinero> query = this.entityManager.createNamedQuery("Cocinero.findAll", Cocinero.class);
        return query.getResultList();
    }


}
