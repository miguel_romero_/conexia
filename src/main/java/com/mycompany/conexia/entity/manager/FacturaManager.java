/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.entity.manager;

import com.mycompany.conexia.entity.Factura;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Stateless
public class FacturaManager {
    
    @PersistenceContext
    private EntityManager entityManager;
   
    public Factura createFactura(Factura factura){
        this.entityManager.persist(factura);
        this.entityManager.flush();
        return factura;
        
    }

}
