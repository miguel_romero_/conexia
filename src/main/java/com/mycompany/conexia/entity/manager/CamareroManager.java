/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.entity.manager;

import com.mycompany.conexia.entity.Camarero;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Stateless
public class CamareroManager {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public Camarero createCamarero(Camarero camarero){
        this.entityManager.persist(camarero);
        this.entityManager.flush();
        return camarero;        
    }
    
    public List<Camarero> findAllCamarero(){
        TypedQuery<Camarero> query = this.entityManager.createNamedQuery("Camarero.findAll", Camarero.class);
        return query.getResultList();
    }

}
