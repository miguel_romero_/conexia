/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.entity.manager;

import com.mycompany.conexia.dto.TotalVentasCamareroDto;
import com.mycompany.conexia.entity.Camarero;
import com.mycompany.conexia.entity.DetalleFactura;
import com.mycompany.conexia.entity.Factura;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Stateless
public class DetalleFacturaManager {
    
    @PersistenceContext
    private EntityManager entityManager;
   
    public DetalleFactura createDetalleFactura(DetalleFactura detalleFactura){
        this.entityManager.persist(detalleFactura);
        this.entityManager.flush();
        return detalleFactura;
        
    }
    
    public List<TotalVentasCamareroDto> getSells() {
        TypedQuery<TotalVentasCamareroDto> query = this.entityManager.createNamedQuery("DetalleFactura.findByCamarero", TotalVentasCamareroDto.class);
        return query.getResultList();
    }

}
