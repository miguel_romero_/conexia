/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.controller;

import com.mycompany.conexia.delegate.CamareroDelegate;
import com.mycompany.conexia.dto.TotalVentasCamareroDto;
import com.mycompany.conexia.entity.Camarero;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.SessionScoped; 
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject; 

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@ManagedBean
@SessionScoped
public class CamareroController implements Serializable {
    
    @Inject
    CamareroDelegate camareroDelegate;
    
    Camarero camarero = new Camarero();
    Camarero camarero2;
    Camarero camareroSelected;
    List<Camarero> camareros = new ArrayList<>();
    List<TotalVentasCamareroDto> ventas = new ArrayList<>();
    
    @PostConstruct
    public void init(){
        updateCamarerosList();
    }
    
    public void showData(Camarero c){
        
        System.out.print(camareroSelected);
        
    }
    
    public void updateCamarerosList(){
        this.camareros = this.camareroDelegate.getAllCamareros();
    }
    
    public void sels(ValueChangeEvent vce){
        this.setCamarero((Camarero)vce.getNewValue());
        this.ventas = this.camareroDelegate.getSells((Camarero)vce.getNewValue());
    }
    
    public void createCamarero(){
        this.camarero2 = this.camareroDelegate.saveCamarero(this.camarero);
        this.updateCamarerosList();
    }
    
    public Camarero getCamarero() {
        return camarero;
    }

    public void setCamarero(Camarero camarero) {
        this.camarero = camarero;
    }

    public List<Camarero> getCamareros() {
        return camareros;
    }

    public void setCamareros(List<Camarero> camareros) {
        this.camareros = camareros;
    }

    public Camarero getCamarero2() {
        return camarero2;
    }

    public void setCamarero2(Camarero camarero2) {
        this.camarero2 = camarero2;
    }

    public Camarero getCamareroSelected() {
        return camareroSelected;
    }

    public void setCamareroSelected(Camarero camareroSelected) {
        this.camareroSelected = camareroSelected;
    }

    public List<TotalVentasCamareroDto> getVentas() {
        return ventas;
    }

    public void setVentas(List<TotalVentasCamareroDto> ventas) {
        this.ventas = ventas;
    }
    
    
    
    
}
