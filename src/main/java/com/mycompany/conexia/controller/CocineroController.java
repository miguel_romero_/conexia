/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.controller;

import com.mycompany.conexia.delegate.CocineroDelegate;
import com.mycompany.conexia.entity.Cocinero;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@ManagedBean
@RequestScoped
public class CocineroController {
    
    @Inject
    CocineroDelegate cocineroDelegate;
    
    Cocinero cocinero = new Cocinero();
    List<Cocinero> cocineros = new ArrayList<>();
    
    @PostConstruct
    public void init(){
        updateMesasCocineros();
    }
    
    public void updateMesasCocineros(){
        this.cocineros = this.cocineroDelegate.getAllCocinero();
    }
    
    public void createCocinero(){
        this.cocineroDelegate.saveCocinero(this.cocinero);
        this.updateMesasCocineros();
    }

    public Cocinero getCocinero() {
        return cocinero;
    }

    public void setCocinero(Cocinero cocinero) {
        this.cocinero = cocinero;
    }

    public List<Cocinero> getCocineros() {
        return cocineros;
    }

    public void setCocineros(List<Cocinero> cocineros) {
        this.cocineros = cocineros;
    }
    
}
