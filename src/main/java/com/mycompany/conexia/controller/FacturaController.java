/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.conexia.controller;

import com.mycompany.conexia.delegate.DetalleFacturaDelegate;
import com.mycompany.conexia.delegate.FacturaDelegate;
import com.mycompany.conexia.entity.Camarero;
import com.mycompany.conexia.entity.Cliente;
import com.mycompany.conexia.entity.DetalleFactura;
import com.mycompany.conexia.entity.Factura;
import com.mycompany.conexia.entity.Mesa;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@ManagedBean
@SessionScoped
public class FacturaController implements Serializable {

    private Cliente idCliente;
    private Camarero idCamarero;
    private Mesa idMesa;
    private List<DetalleFactura> detallesFacturas = new ArrayList<>();
    private Date fecha;
    
    @Inject
    FacturaDelegate facturaDelegate;
    
    @Inject
    DetalleFacturaDelegate detalleFacturaDelegate;

    public void showdata() {

        List<FacesMessage> messages = new ArrayList<>();

        String message = "Debe llenar los siguientes campos:";
        boolean showMessage = false;
        boolean platesFilled = true;
        if (idCliente == null) {
            messages.add(new FacesMessage("datos de cliente", ""));
            showMessage = true;
        }
        if (idCamarero == null) {
            messages.add(new FacesMessage("seleccionar camarero", ""));
            showMessage = true;
        }
        if (idMesa == null) {
            messages.add(new FacesMessage("seleccionar mesa", ""));
            showMessage = true;
        }
        if (detallesFacturas.size() <= 0) {
            messages.add(new FacesMessage("ingresar platos", ""));
            showMessage = true;
        } else {
            for (DetalleFactura detallesFactura : detallesFacturas) {

                if (!platesFilled) {
                    continue;
                }
                if (detallesFactura.getIdCocinero() == null
                        || detallesFactura.getImporte() == 0
                        || detallesFactura.getPlato().isEmpty()) {
                    platesFilled = false;
                    showMessage = true;
                    messages.add(new FacesMessage("llenar los campos de los platos", ""));

                }
            }
        }
        if (showMessage) {
            FacesContext context = FacesContext.getCurrentInstance();

            context.addMessage(null, new FacesMessage(message, ""));
            for (FacesMessage message1 : messages) {
                context.addMessage(null, message1);
            }
        } else {
            
            Factura f = new Factura();
            f.setIdCliente(idCliente);
            f.setFechaFactura(fecha);
            f.setIdCamarero(idCamarero);
            f.setIdMesa(idMesa);
            
            f = facturaDelegate.savefactura(f);
            
            for (DetalleFactura detallesFactura : detallesFacturas) {
                detallesFactura.setIdFactura(f);
                detalleFacturaDelegate.savefactura(detallesFactura);
            }
            
            FacesContext context = FacesContext.getCurrentInstance();

            context.addMessage(null, new FacesMessage("Se guardo la informacion correctamente", ""));
            
            
        }

    }

    @PostConstruct
    public void init() {
        detallesFacturas = new ArrayList<>();
        DetalleFactura detalle = new DetalleFactura();
        this.detallesFacturas.add(detalle);
    }

    public void addDetalleFactura() {
        DetalleFactura detalle = new DetalleFactura();
        detallesFacturas.add(detalle);
    }

    public void removeDetalleFactura() {
        detallesFacturas.remove(detallesFacturas.size() - 1);
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public Camarero getIdCamarero() {
        return idCamarero;
    }

    public void setIdCamarero(Camarero idCamarero) {
        this.idCamarero = idCamarero;
        System.out.println(idCamarero);
    }

    public Mesa getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(Mesa idMesa) {
        this.idMesa = idMesa;
        System.out.println(idMesa);
    }

    public List<DetalleFactura> getDetallesFacturas() {
        return detallesFacturas;
    }

    public void setDetallesFacturas(List<DetalleFactura> detallesFacturas) {
        this.detallesFacturas = detallesFacturas;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    
}
