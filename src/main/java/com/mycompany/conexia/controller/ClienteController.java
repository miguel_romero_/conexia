/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.conexia.controller;

import com.mycompany.conexia.delegate.ClienteDelegate;
import com.mycompany.conexia.entity.Cliente;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@ManagedBean
@RequestScoped
public class ClienteController {

    @Inject
    ClienteDelegate clienteDelegate;

    private Cliente cliente = new Cliente();
    private Cliente cliente2;
    private Map<Cliente, Long> buenosClientes = new HashMap<>();
    private List<Cliente> keys = new ArrayList<>();

    public void saveCliente() {
        this.cliente2 = this.clienteDelegate.saveCliente(this.cliente);
    }

    public void findGoodClients() {
        this.buenosClientes = this.clienteDelegate.getGoogClients();
        for (Map.Entry<Cliente, Long> entry : this.buenosClientes.entrySet()) {
            keys.add(entry.getKey()); 
        } 
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente2() {
        return cliente2;
    }

    public void setCliente2(Cliente cliente2) {
        this.cliente2 = cliente2;
    }

    public Map<Cliente, Long> getBuenosClientes() {
        return buenosClientes;
    }

    public void setBuenosClientes(Map<Cliente, Long> buenosClientes) {
        this.buenosClientes = buenosClientes;
    }

    public List<Cliente> getKeys() {
        return keys;
    }

    public void setKeys(List<Cliente> keys) {
        this.keys = keys;
    }

    
}
