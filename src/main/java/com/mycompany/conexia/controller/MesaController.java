/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.controller;

import com.mycompany.conexia.delegate.MesaDelegate;
import com.mycompany.conexia.entity.Mesa;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@ManagedBean
@RequestScoped
public class MesaController {
    
    
    @Inject
    MesaDelegate mesaDelegate;
    
    Mesa mesa = new Mesa(); 
    Mesa mesa2; 
    List<Mesa> mesas = new ArrayList<>();
    
    @PostConstruct
    public void init(){
        updateMesasList();
    }
    
    public void updateMesasList(){
        this.mesas = mesaDelegate.getAllMesa();
    }
    
    public void createMesa(){
        this.mesa2 = this.mesaDelegate.saveMesa(this.mesa);
        this.updateMesasList();
    }

    public Mesa getMesa() {
        return mesa;
    }

    public void setMesa(Mesa mesa) {
        this.mesa = mesa;
    }

    public Mesa getMesa2() {
        return mesa2;
    }

    public void setMesa2(Mesa mesa2) {
        this.mesa2 = mesa2;
    }

    public List<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(List<Mesa> mesas) {
        this.mesas = mesas;
    }

    
}
