/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.delegate;

import com.mycompany.conexia.entity.Mesa;
import com.mycompany.conexia.entity.manager.MesaManager;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Stateless
public class MesaDelegate {

    @Inject
    MesaManager mesaManager;
    
    public Mesa saveMesa(Mesa mesa){
        return this.mesaManager.createMesa(mesa);                
    }
    
    public List<Mesa> getAllMesa(){
        return this.mesaManager.findAllMesa();
    }
}
