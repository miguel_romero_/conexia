/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.conexia.delegate;

import com.mycompany.conexia.entity.Cocinero;
import com.mycompany.conexia.entity.manager.CocineroManager;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Stateless
public class CocineroDelegate {
    
    @Inject
    CocineroManager cocineroManager;
    
    public Cocinero saveCocinero(Cocinero cocinero){
        return this.cocineroManager.createCocinero(cocinero);                
    }
    
    public List<Cocinero> getAllCocinero(){
        return this.cocineroManager.findAllCocinero();
    }

}
