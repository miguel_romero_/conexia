/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.conexia.delegate;

import com.mycompany.conexia.dto.TotalVentasCamareroDto;
import com.mycompany.conexia.entity.Camarero;
import com.mycompany.conexia.entity.manager.CamareroManager;
import com.mycompany.conexia.entity.manager.DetalleFacturaManager;
import com.mycompany.conexia.utils.DateUtils;
import java.security.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Stateless
public class CamareroDelegate {

    @Inject
    CamareroManager camareroManager;

    @Inject
    DetalleFacturaManager detalleFacturaManager;

    @PersistenceContext
    private EntityManager entityManager;

    public Camarero saveCamarero(Camarero camarero) {
        return this.camareroManager.createCamarero(camarero);
    }

    public List<TotalVentasCamareroDto> getSells(Camarero camarero) {

        List<Object[]> rawResultList;
        Query query = entityManager.createNativeQuery(
                "SELECT SUM(res.suma) as cantidad, MONTH( res.fechaFactura ) AS mes "
                + "FROM "
                + "(SELECT "
                + "SUM(Importe) AS suma, b.fechaFactura "
                + "FROM DetalleFactura a "
                + "RIGHT JOIN Factura b "
                + "on a.idFactura = b.idFactura "
                + "where b.idCamarero = ? "
                + "GROUP BY b.idFactura) "
                + "AS res "
                + "GROUP BY mes")
                .setParameter(1, camarero.getIdCamarero());
        rawResultList = query.getResultList();

        List<TotalVentasCamareroDto> postDTOs = new ArrayList<>();
        if (!rawResultList.isEmpty()) {
            for (Object[] object : rawResultList) {
                postDTOs.add(new TotalVentasCamareroDto(DateUtils.getMonthName(Integer.parseInt(object[1].toString())), camarero, Long.valueOf(object[0].toString())));
            }
        }

        return postDTOs;
    }

    public List<Camarero> getAllCamareros() {
        return this.camareroManager.findAllCamarero();
    }
}
