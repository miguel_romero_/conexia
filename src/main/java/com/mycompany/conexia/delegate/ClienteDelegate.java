/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.delegate;

import com.mycompany.conexia.entity.Cliente;
import com.mycompany.conexia.entity.manager.ClienteManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Stateless
public class ClienteDelegate {
    
    @Inject
    ClienteManager clienteManager;
    
    public Cliente saveCliente(Cliente cliente){
        Cliente c = this.clienteManager.createCliente(cliente); 
        return c;
    } 
    
    public Map<Cliente,Long> getGoogClients(){
        List<Cliente> clientes = this.clienteManager.getGoodClients(); 
        Map<Cliente,Long> finales = new HashMap<>();
        for (Object cliente : clientes) {
            Object[] obj = (Object[]) cliente; 
            if(Long.parseLong(obj[0].toString()) >= 100000L ){
                finales.put((Cliente)obj[1], Long.parseLong(obj[0].toString()));
            }
        }
        return finales;
    }

}
