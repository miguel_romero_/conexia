/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.conexia.delegate;

import com.mycompany.conexia.entity.DetalleFactura;
import com.mycompany.conexia.entity.Factura;
import com.mycompany.conexia.entity.manager.DetalleFacturaManager;
import com.mycompany.conexia.entity.manager.FacturaManager;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Stateless
public class DetalleFacturaDelegate {

    @Inject
    DetalleFacturaManager detalleFacturaManager;
    
    public DetalleFactura savefactura(DetalleFactura factura){
        DetalleFactura f = this.detalleFacturaManager.createDetalleFactura(factura);
        return f;
    }
}
