package com.mycompany.conexia.entity;

import com.mycompany.conexia.entity.Cocinero;
import com.mycompany.conexia.entity.Factura;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-14T11:29:10")
@StaticMetamodel(DetalleFactura.class)
public class DetalleFactura_ { 

    public static volatile SingularAttribute<DetalleFactura, Factura> idFactura;
    public static volatile SingularAttribute<DetalleFactura, Cocinero> idCocinero;
    public static volatile SingularAttribute<DetalleFactura, Integer> idDetalleFactura;
    public static volatile SingularAttribute<DetalleFactura, String> plato;
    public static volatile SingularAttribute<DetalleFactura, Integer> importe;

}