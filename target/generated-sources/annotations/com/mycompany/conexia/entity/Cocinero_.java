package com.mycompany.conexia.entity;

import com.mycompany.conexia.entity.DetalleFactura;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-14T11:29:10")
@StaticMetamodel(Cocinero.class)
public class Cocinero_ { 

    public static volatile SingularAttribute<Cocinero, String> apellido2;
    public static volatile SingularAttribute<Cocinero, DetalleFactura> detalleFactura;
    public static volatile SingularAttribute<Cocinero, String> apellido1;
    public static volatile SingularAttribute<Cocinero, Integer> idCocinero;
    public static volatile SingularAttribute<Cocinero, String> nombre;

}