package com.mycompany.conexia.entity;

import com.mycompany.conexia.entity.Camarero;
import com.mycompany.conexia.entity.Cliente;
import com.mycompany.conexia.entity.DetalleFactura;
import com.mycompany.conexia.entity.Mesa;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-14T11:29:10")
@StaticMetamodel(Factura.class)
public class Factura_ { 

    public static volatile ListAttribute<Factura, DetalleFactura> detalleFactura;
    public static volatile SingularAttribute<Factura, Mesa> idMesa;
    public static volatile SingularAttribute<Factura, Cliente> idCliente;
    public static volatile SingularAttribute<Factura, Integer> idFactura;
    public static volatile SingularAttribute<Factura, Date> fechaFactura;
    public static volatile SingularAttribute<Factura, Camarero> idCamarero;

}